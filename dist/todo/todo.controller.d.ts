import { AddTodoItemDto } from './@types/dto/addTodoItem.dto';
import { Todo } from './@types/entity/todo.entity';
import { TodoService } from './todo.service';
export declare class TodoController {
    private readonly todoService;
    constructor(todoService: TodoService);
    getTodoList(queryString: string): Promise<Todo[]>;
    getTodoItem(todoId: string): Promise<Todo>;
    addTodoItem(addTodoItemDto: AddTodoItemDto): Promise<Todo>;
    makeTodoItemComplete(todoId: string): Promise<void>;
    makeTodoItemUncomplete(todoId: string): Promise<void>;
    deleteTodoItem(todoId: string): Promise<void>;
}
