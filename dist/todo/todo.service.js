"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoService = void 0;
const common_1 = require("@nestjs/common");
const todo_repository_1 = require("./@types/repository/todo.repository");
let TodoService = class TodoService {
    constructor(todoRepository) {
        this.todoRepository = todoRepository;
    }
    getTodoList(queryString) {
        return this.todoRepository.getTodoList(queryString);
    }
    getTodoItem(todoId) {
        return this.todoRepository.getTodoItem(todoId);
    }
    addTodoItem(addTodoItemDto) {
        return this.todoRepository.addTodoItem(addTodoItemDto.title, addTodoItemDto.description, addTodoItemDto.completed);
    }
    async makeTodoItemComplete(todoId) {
        await this.todoRepository.makeTodoItemComplete(todoId);
    }
    async makeTodoItemUncomplete(todoId) {
        await this.todoRepository.makeTodoItemUncomplete(todoId);
    }
    async deleteTodoItem(todoId) {
        await this.todoRepository.deleteTodoItem(todoId);
    }
};
TodoService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [todo_repository_1.TodoRepository])
], TodoService);
exports.TodoService = TodoService;
//# sourceMappingURL=todo.service.js.map