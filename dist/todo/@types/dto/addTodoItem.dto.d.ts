export declare class AddTodoItemDto {
    readonly title: string;
    readonly description: string;
    readonly completed: boolean;
}
