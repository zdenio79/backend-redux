import { Repository } from "typeorm";
import { Todo } from '../entity/todo.entity';
export declare class TodoRepository extends Repository<Todo> {
    getTodoList(queryString?: string, sortColumn?: string, sortOrder?: string): Promise<Todo[]>;
    getTodoItem(todoId: string): Promise<Todo>;
    addTodoItem(title: string, description: string, completed?: boolean): Promise<Todo>;
    makeTodoItemComplete(todoId: string): Promise<void>;
    makeTodoItemUncomplete(todoId: string): Promise<void>;
    deleteTodoItem(todoId: string): Promise<void>;
}
