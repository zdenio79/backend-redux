import { AddTodoItemDto } from './@types/dto/addTodoItem.dto';
import { Todo } from './@types/entity/todo.entity';
import { TodoRepository } from './@types/repository/todo.repository';
export declare class TodoService {
    private todoRepository;
    constructor(todoRepository: TodoRepository);
    getTodoList(queryString: string): Promise<Todo[]>;
    getTodoItem(todoId: string): Promise<Todo>;
    addTodoItem(addTodoItemDto: AddTodoItemDto): Promise<Todo>;
    makeTodoItemComplete(todoId: string): Promise<void>;
    makeTodoItemUncomplete(todoId: string): Promise<void>;
    deleteTodoItem(todoId: string): Promise<void>;
}
