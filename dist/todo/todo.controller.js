"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoController = void 0;
const common_1 = require("@nestjs/common");
const addTodoItem_dto_1 = require("./@types/dto/addTodoItem.dto");
const todo_service_1 = require("./todo.service");
let TodoController = class TodoController {
    constructor(todoService) {
        this.todoService = todoService;
    }
    getTodoList(queryString) {
        return this.todoService.getTodoList(queryString);
    }
    getTodoItem(todoId) {
        return this.todoService.getTodoItem(todoId);
    }
    addTodoItem(addTodoItemDto) {
        return this.todoService.addTodoItem(addTodoItemDto);
    }
    makeTodoItemComplete(todoId) {
        return this.todoService.makeTodoItemComplete(todoId);
    }
    makeTodoItemUncomplete(todoId) {
        return this.todoService.makeTodoItemUncomplete(todoId);
    }
    deleteTodoItem(todoId) {
        return this.todoService.deleteTodoItem(todoId);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query('q')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TodoController.prototype, "getTodoList", null);
__decorate([
    common_1.Get(':todoId'),
    __param(0, common_1.Param('todoId', new common_1.ParseUUIDPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TodoController.prototype, "getTodoItem", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [addTodoItem_dto_1.AddTodoItemDto]),
    __metadata("design:returntype", Promise)
], TodoController.prototype, "addTodoItem", null);
__decorate([
    common_1.Put(':todoId/complete'),
    __param(0, common_1.Param('todoId', new common_1.ParseUUIDPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TodoController.prototype, "makeTodoItemComplete", null);
__decorate([
    common_1.Put(':todoId/uncomplete'),
    __param(0, common_1.Param('todoId', new common_1.ParseUUIDPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TodoController.prototype, "makeTodoItemUncomplete", null);
__decorate([
    common_1.Delete(':todoId'),
    __param(0, common_1.Param('todoId', new common_1.ParseUUIDPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TodoController.prototype, "deleteTodoItem", null);
TodoController = __decorate([
    common_1.Controller('todo'),
    __metadata("design:paramtypes", [todo_service_1.TodoService])
], TodoController);
exports.TodoController = TodoController;
//# sourceMappingURL=todo.controller.js.map