import { Injectable } from '@nestjs/common';
import { AddTodoItemDto } from './@types/dto/addTodoItem.dto';
import { Todo } from './@types/entity/todo.entity';
import { TodoRepository } from './@types/repository/todo.repository';

@Injectable()
export class TodoService {
    constructor(private todoRepository: TodoRepository) {
    }

    public getTodoList(queryString: string): Promise<Todo[]> { 
        return this.todoRepository.getTodoList(queryString);
    }

    public getTodoItem(todoId: string): Promise<Todo> {
        return this.todoRepository.getTodoItem(todoId);
    }

    public addTodoItem(addTodoItemDto: AddTodoItemDto): Promise<Todo> {
        return this.todoRepository.addTodoItem(
            addTodoItemDto.title,
            addTodoItemDto.description, 
            addTodoItemDto.completed
        );
    }

    public async makeTodoItemComplete(todoId: string): Promise<void> {
        await this.todoRepository.makeTodoItemComplete(todoId);
    }

    public async makeTodoItemUncomplete(todoId: string): Promise<void> {
        await this.todoRepository.makeTodoItemUncomplete(todoId);
    }

    public async deleteTodoItem(todoId: string): Promise<void> {
        await this.todoRepository.deleteTodoItem(todoId);
    }

}
