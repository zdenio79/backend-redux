import { Controller, Get, Post, Put, Delete, Param, ParseUUIDPipe, Body, Query } from '@nestjs/common';
import { AddTodoItemDto } from './@types/dto/addTodoItem.dto';
import { Todo } from './@types/entity/todo.entity';
import { TodoService } from './todo.service';

@Controller('todo')
export class TodoController {
    constructor(private readonly todoService: TodoService) {
    }

    // URL?queryParamName=queryParamValue
    // ?q=tresc-wyszukania
    @Get() 
    public getTodoList(@Query('q') queryString: string): Promise<Todo[]> { 
        return this.todoService.getTodoList(queryString);
    }

    @Get(':todoId')
    public getTodoItem(@Param('todoId', new ParseUUIDPipe()) todoId: string): Promise<Todo> {
        return this.todoService.getTodoItem(todoId);
    }

    @Post()
    public addTodoItem(@Body() addTodoItemDto: AddTodoItemDto): Promise<Todo> {
        return this.todoService.addTodoItem(addTodoItemDto);
    }

    @Put(':todoId/complete')
    public makeTodoItemComplete(@Param('todoId', new ParseUUIDPipe()) todoId: string): Promise<void> {
        return this.todoService.makeTodoItemComplete(todoId);
    }

    @Put(':todoId/uncomplete')
    public makeTodoItemUncomplete(@Param('todoId', new ParseUUIDPipe()) todoId: string): Promise<void> {
        return this.todoService.makeTodoItemUncomplete(todoId);
    }

    @Delete(':todoId')
    public deleteTodoItem(@Param('todoId', new ParseUUIDPipe()) todoId: string): Promise<void> {
        return this.todoService.deleteTodoItem(todoId);
    }

}
