import { EntityRepository, Repository } from "typeorm";
import { Todo } from '../entity/todo.entity'

@EntityRepository(Todo)
export class TodoRepository extends Repository<Todo> {

    public getTodoList(queryString: string = '', sortColumn: string = 'createdAt', sortOrder: string = 'DESC'): Promise<Todo[]> {
        // const qb = this.todoRepository.createQueryBuilder('todo');
        const qb = this.createQueryBuilder('todo');

        if(queryString) {
            qb.where("todo.title LIKE :param", { param:`%${queryString}%` })
                .orWhere('todo.description LIKE :param', { param:`%${queryString}%` })
        }

        qb.orderBy(`todo.${sortColumn}`, sortOrder === 'ASC' ? 'ASC' : 'DESC');

        return qb.getMany();
    }

    public getTodoItem(todoId: string): Promise<Todo> {
        return this.findOne(todoId);
    }

    public addTodoItem(title: string, description: string, completed: boolean = false): Promise<Todo> {
        return this.save({
            title,
            description,
            completed,
        });
    }

    public async makeTodoItemComplete(todoId: string): Promise<void> {
        await this.update({ 
            id: todoId,
        }, {
            completed: true,
        });
    }

    public async makeTodoItemUncomplete(todoId: string): Promise<void> {
        await this.update({ 
            id: todoId,
        }, {
            completed: false,
        });
    }

    public async deleteTodoItem(todoId: string): Promise<void> {
        await this.softDelete({
            id: todoId
        });
    }

}