import { IsBoolean, IsOptional, IsString, Length, MaxLength } from "class-validator";

export class AddTodoItemDto {
    @IsString()
    @Length(3, 255)
    public readonly title: string;

    @IsString()
    @MaxLength(500)
    public readonly description: string;

    @IsBoolean()
    @IsOptional()
    public readonly completed: boolean; 
}